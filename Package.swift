// swift-tools-version: 5.8
// The swift-tools-version declares the minimum version of Swift required to build this package.

import PackageDescription

let package = Package(
	name: "tilesearch",
	platforms: [
		.macOS(.v13),
	],
	products: [
	 .executable(
		 name: "tilesearch",
		 targets: ["tilesearch"])
	],
	dependencies: [
		// Dependencies declare other packages that this package depends on.
		// .package(url: /* package url */, from: "1.0.0"),
		.package(url: "https://github.com/apple/swift-argument-parser.git", exact: "1.2.2"),
		.package(url: "https://github.com/1024jp/GzipSwift", exact: "6.0.1"),
		.package(url: "https://github.com/apple/swift-protobuf.git", exact: "1.22.0"),
	],
	targets: [
		// Targets are the basic building blocks of a package, defining a module or a test suite.
		// Targets can depend on other targets in this package and products from dependencies.
		.executableTarget(
			name: "tilesearch",
			dependencies: [
				.product(name: "ArgumentParser", package: "swift-argument-parser"),
				.product(name: "Gzip", package: "GzipSwift"),
				.product(name: "SwiftProtobuf", package: "swift-protobuf"),
			],
			path: "Sources"
		)
	]
)
