#  tilesearch

## Usage

First, use https://boundingbox.klokantech.com to find the search bounds. Be sure to choose "CSV" mode.

You will also need a mbtiles file with detail to a suitable zoom level.

This example searches `york.mbtiles` for the restaurant named Döner Summer:

```
swift run tilesearch --tiles ./york.mbtiles --bounds "-1.122517,53.937521,-1.038959,53.983318" --zoom 14 "döner"
```

## Output

```
Opened file ./york.mbtiles
Searching tile columns 8140-8144, rows 11122-11118
Total feature count: 105156
Found 5 with the search term döner
Feature(tags: ["name:latin": "Döner Summer", "name": "Döner Summer", "subclass": "fast_food", "name_en": "Döner Summer", "name_int": "Döner Summer", "name_de": "Döner Summer", "class": "fast_food"], tile: tilesearch.Tile(tms: true, zoom: 14, column: 8142, row: 11119, origin: __C.CLLocationCoordinate2D(latitude: 53.95608553098789, longitude: -1.0986328125), width: 0.02197265625, height: -0.012930828738596745), geometry: tilesearch.FeatureGeometry(components: [tilesearch.FeatureGeometryComponent.moveTo(__C.CLLocationCoordinate2D(latitude: 53.956969474358694, longitude: -1.0778510570526123))]))
Feature(tags: ["name_int": "Döner Summer", "name:latin": "Döner Summer", "class": "fast_food", "name": "Döner Summer", "subclass": "fast_food", "name_en": "Döner Summer", "name_de": "Döner Summer"], tile: tilesearch.Tile(tms: true, zoom: 14, column: 8142, row: 11120, origin: __C.CLLocationCoordinate2D(latitude: 53.9690123507403, longitude: -1.0986328125), width: 0.02197265625, height: -0.012926819752408392), geometry: tilesearch.FeatureGeometry(components: [tilesearch.FeatureGeometryComponent.moveTo(__C.CLLocationCoordinate2D(latitude: 53.95697235626876, longitude: -1.0778510570526123))]))
Feature(tags: ["subclass": "cafe", "name_int": "Döner Summer", "name_en": "Döner Summer", "name": "Döner Summer", "name_de": "Döner Summer", "name:latin": "Döner Summer", "class": "cafe"], tile: tilesearch.Tile(tms: true, zoom: 14, column: 8142, row: 11120, origin: __C.CLLocationCoordinate2D(latitude: 53.9690123507403, longitude: -1.0986328125), width: 0.02197265625, height: -0.012926819752408392), geometry: tilesearch.FeatureGeometry(components: [tilesearch.FeatureGeometryComponent.moveTo(__C.CLLocationCoordinate2D(latitude: 53.96391231638486, longitude: -1.0841220617294312))]))
Feature(tags: ["name:latin": "Döner Summer", "name_en": "Döner Summer", "name": "Döner Summer", "name_int": "Döner Summer", "class": "fast_food", "name_de": "Döner Summer", "subclass": "fast_food"], tile: tilesearch.Tile(tms: true, zoom: 14, column: 8143, row: 11119, origin: __C.CLLocationCoordinate2D(latitude: 53.95608553098789, longitude: -1.07666015625), width: 0.02197265625, height: -0.012930828738596745), geometry: tilesearch.FeatureGeometry(components: [tilesearch.FeatureGeometryComponent.moveTo(__C.CLLocationCoordinate2D(latitude: 53.956969474358694, longitude: -1.0778456926345825))]))
Feature(tags: ["name_int": "Döner Summer", "name_de": "Döner Summer", "name_en": "Döner Summer", "name": "Döner Summer", "class": "fast_food", "subclass": "fast_food", "name:latin": "Döner Summer"], tile: tilesearch.Tile(tms: true, zoom: 14, column: 8143, row: 11120, origin: __C.CLLocationCoordinate2D(latitude: 53.9690123507403, longitude: -1.07666015625), width: 0.02197265625, height: -0.012926819752408392), geometry: tilesearch.FeatureGeometry(components: [tilesearch.FeatureGeometryComponent.moveTo(__C.CLLocationCoordinate2D(latitude: 53.95697235626876, longitude: -1.0778456926345825))]))
```

## Duplicate search results

The results may contain duplicate items. I _think_ this is because the feature exists in multiple tiles, even though it's only _physically_ in the area defined by a single tile's geometry. A tile can contain features which live outside the strict bounds of the tile.

You'll need to do some de-duplication.

## Centre-point of a feature

The `Feature.geometry` property contains a collection of coortinates that describe a point, way or polygon. (Points have a single `moveTo` coordinate, ways add a number of `lineTo` coordinates, and polygons add a `closePath` item)

If I needed to find the approximate midpoint of a way, I'd take the median coordinate in the geometry collection. For the midpoint in a polygon I'd take an average of all the coordinates in the collection.

## Note about protobuf

The source file `vector_tile.pb.swift` was generated using version 1.22.0 of _swift-protobuf_. 
