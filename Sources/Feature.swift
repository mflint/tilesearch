import Foundation

struct Feature {
	let tags: [String : String]
	let tile: Tile
	let geometry: FeatureGeometry

	init(feature: Mapboxgl_Vector_feature,
		 layer: Mapboxgl_Vector_layer,
		 tile: Tile) throws {
		var tags = [String : String]()
		for tagIndex in stride(from: 0, to: feature.tags.count, by: 2) {
			let keyIndex = Int(feature.tags[tagIndex])
			let valueIndex = Int(feature.tags[tagIndex + 1])
			let key = layer.keys[keyIndex]
			let value = layer.values[valueIndex]

			// note: this currently ignores non-string tags
			if value.hasStringValue {
				tags[key] = value.stringValue
			}
		}
		self.tags = tags
		self.tile = tile
		self.geometry = try FeatureGeometry(from: feature.geometry, in: tile, extent: layer.extent)
	}
}
