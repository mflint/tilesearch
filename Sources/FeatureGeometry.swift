import Foundation
import CoreLocation

private extension CLLocationCoordinate2D {
	func adjusted(dx: Double, dy: Double) -> CLLocationCoordinate2D {
		CLLocationCoordinate2D(latitude: self.latitude + dy,
							   longitude: self.longitude + dx)
	}
}

enum FeatureGeometryComponent {
	case moveTo(CLLocationCoordinate2D)
	case lineTo(CLLocationCoordinate2D)
	case closePath
}

struct FeatureGeometry {
	enum FeatureGeometryError: Error {
		case unexpectedGeometryCount(UInt32)
		case unexpectedGeometryCommand(UInt32)
	}

	let components: [FeatureGeometryComponent]

	init(from geometry: [UInt32],
		 in tile: Tile,
		 extent: UInt32) throws {
		// https://github.com/mapbox/vector-tile-spec/tree/master/2.1#41-layers
		func value(parameter: UInt32) -> Int32 {
			parameter & 1 == 0 ? Int32(parameter >> 1) : -Int32(parameter >> 1)
		}

		var cursor = tile.origin
		var components = [FeatureGeometryComponent]()

		var input = geometry
		while !input.isEmpty {
			let commandInt = input.removeFirst()
			let command = commandInt & 7
			let count = commandInt >> 3
			guard count > 0 else {
				throw FeatureGeometryError.unexpectedGeometryCount(count)
			}

			switch command {
			case 1:
				// move
				for _ in 0..<count {
					let p1 = input.removeFirst()
					let p2 = input.removeFirst()
					let z1 = value(parameter: p1)
					let z2 = value(parameter: p2)
					let dx = Double(z1) * tile.width / Double(extent)
					let dy = Double(z2) * tile.height / Double(extent)
					let coordinate = cursor.adjusted(dx: dx, dy: dy)
					components.append(.moveTo(coordinate))
					cursor = coordinate
				}
			case 2:
				// line
				for _ in 0..<count {
					let p1 = input.removeFirst()
					let p2 = input.removeFirst()
					let z1 = value(parameter: p1)
					let z2 = value(parameter: p2)
					let dx = Double(z1) * tile.width / Double(extent)
					let dy = Double(z2) * tile.height / Double(extent)
					let coordinate = cursor.adjusted(dx: dx, dy: dy)
					components.append(.lineTo(coordinate))
					cursor = coordinate
				}
			case 7:
				// close path
				components.append(.closePath)
			default:
				throw FeatureGeometryError.unexpectedGeometryCommand(command)
			}
		}

		self.components = components
	}
}
