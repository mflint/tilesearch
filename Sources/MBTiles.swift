import Foundation
import CoreLocation
import SQLite3
import Gzip

class MBTiles {
	enum MBTilesError: Error {
		case couldNotOpenFile
		case badZoom
		case badBounds
		case couldNotPrepareStatement
	}

	// pointer to the open database
	private let db: OpaquePointer

	init(tilesPath: String) throws {
		var db: OpaquePointer?
		guard sqlite3_open(tilesPath, &db) == SQLITE_OK,
			  let db else {
			throw MBTilesError.couldNotOpenFile
		}
		self.db = db

		print("Opened file \(tilesPath)")
	}

	deinit {
		sqlite3_close(self.db)
	}

	func features(searchBounds: String,
				searchZoom zoomString: String) throws -> [Feature] {
		guard let searchZoom = Int(zoomString),
			  searchZoom > 0 else {
			throw MBTilesError.badZoom
		}

		// convert the bounds string into tile references
		let (northEastTile, southWestTile) = try self.tiles(for: searchBounds, searchZoom: searchZoom)
		print("Searching tile columns \(southWestTile.column)-\(northEastTile.column), rows \(northEastTile.row)-\(southWestTile.row)")

		return try self.features(northEastTile: northEastTile,
											   southWestTile: southWestTile,
											   searchZoom: searchZoom)
	}

	private func tiles(for searchBounds: String, searchZoom: Int) throws -> (Tile, Tile) {
		let boundComponents = searchBounds.components(separatedBy: ",")
		let bounds = boundComponents.compactMap { Double($0) }
		guard bounds.count == 4 else {
			throw MBTilesError.badBounds
		}

		let southWestCorner = CLLocationCoordinate2D(latitude: bounds[1], longitude: bounds[0])
		let northEastCorner = CLLocationCoordinate2D(latitude: bounds[3], longitude: bounds[2])

		let southWestTile = Tile(coordinate: southWestCorner, zoom: searchZoom)
		let northEastTile = Tile(coordinate: northEastCorner, zoom: searchZoom)

		return (northEastTile, southWestTile)
	}

	private func features(northEastTile: Tile, southWestTile: Tile, searchZoom: Int) throws -> [Feature] {
		var queryStatement: OpaquePointer?
		guard sqlite3_prepare_v2(db, "SELECT * FROM tiles WHERE zoom_level = ? AND tile_column >= ? AND tile_column <= ? AND tile_row >= ? AND tile_row <= ?;", -1, &queryStatement, nil) == SQLITE_OK else {
			throw MBTilesError.couldNotPrepareStatement
		}

		defer {
			sqlite3_finalize(queryStatement)
		}

		sqlite3_bind_int(queryStatement, 1, Int32(searchZoom))
		sqlite3_bind_int(queryStatement, 2, Int32(southWestTile.column))
		sqlite3_bind_int(queryStatement, 3, Int32(northEastTile.column))
		sqlite3_bind_int(queryStatement, 4, Int32(southWestTile.row))
		sqlite3_bind_int(queryStatement, 5, Int32(northEastTile.row))

		var features = [Feature]()

		while sqlite3_step(queryStatement) == SQLITE_ROW {
			let zoom = Int(sqlite3_column_int(queryStatement, 0))
			let column = Int(sqlite3_column_int(queryStatement, 1))
			let row = Int(sqlite3_column_int(queryStatement, 2))
			let tileGeometry = Tile(tms: true, zoom: zoom, column: column, row: row)
			let dataBlobLength = sqlite3_column_bytes(queryStatement, 3)

			if let dataBlob = sqlite3_column_blob(queryStatement, 3),
			   let zippedData = Optional(Data(bytes: dataBlob, count: Int(dataBlobLength))),
			   let data = try? zippedData.gunzipped(),
			   let tile = try? Mapboxgl_Vector_tile(serializedData: data as Data) {
				for layer in tile.layers {
					let newFeatures = try layer.features.map { feature in
						try Feature(feature: feature,
									layer: layer,
									tile: tileGeometry)
					}
					features.append(contentsOf: newFeatures)
				}
			}
		}

		return features
	}
}
