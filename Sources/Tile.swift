import Foundation
import CoreLocation

struct Tile {
	let tms: Bool
	let zoom: Int
	let column: Int
	let row: Int

	let origin: CLLocationCoordinate2D
	let width: CLLocationDegrees
	let height: CLLocationDegrees

	/// Creates a Tile from a lat/lon coordinate and zoom level
	/// - Parameters:
	///   - coordinate:
	///   - zoom:
	///   - tms: Tile Map Service. MBTiles use TMS, so this should be true. See https://wiki.openstreetmap.org/wiki/TMS
	init(coordinate: CLLocationCoordinate2D, zoom: Int, tms: Bool = true) {
		let column = floor((coordinate.longitude + 180) / 360.0 * pow(2.0, Double(zoom)))

		var row = (floor((1 - log( tan( coordinate.latitude * Double.pi / 180.0 ) + 1 / cos(coordinate.latitude * Double.pi / 180.0 )) / Double.pi ) / 2 * pow(2.0, Double(zoom))))

		if tms {
			row = pow(2.0, Double(zoom)) - row - 1
		}

		self.init(tms: tms, zoom: zoom, column: Int(column), row: Int(row))
	}

	init(tms: Bool, zoom: Int, column: Int, row: Int) {
		self.tms = tms
		self.zoom = zoom
		self.column = column
		self.row = row

		func tile2long(x: Double, z: Double) -> CLLocationDegrees {
			   return x / pow(2.0, z) * 360.0 - 180.0
		   }

		   func tile2lat(y: Double, z: Double) -> CLLocationDegrees {
			   let n = Double.pi - (2.0 * Double.pi * y) / pow(2.0, z)
			   return (180 / .pi * atan(0.5 * (exp(n)-exp(-n))))
		   }

		   let x = column
		   let y: Int
		   if tms {
			   y = (1 << zoom) - row - 1
		   } else {
			   y = row
		   }

		   let originLat = tile2lat(y: Double(y), z: Double(zoom))
		   let originLon = tile2long(x: Double(x), z: Double(zoom))
		   self.origin = CLLocationCoordinate2D(latitude: originLat,
												longitude: originLon)
		   self.height = tile2lat(y: Double(y + 1), z: Double(zoom)) - originLat
		   self.width = tile2long(x: Double(x + 1), z: Double(zoom)) - originLon
	}
}
