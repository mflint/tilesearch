import ArgumentParser

@main
struct TileSearch: ParsableCommand {
	@Option(name: .shortAndLong, help: "path to mbtiles file")
	private var tiles: String

	@Option(name: .shortAndLong, parsing: .unconditional, help: "bounds to search (csv: west, south, east, north)", completion: nil)
	private var bounds: String

	@Option(name: .shortAndLong, help: "zoom level to search")
	private var zoom: String

	@Argument(help: "value to search for")
	private var term: String

	func run() throws {
		let allFeatures = try MBTiles(tilesPath: self.tiles)
			.features(searchBounds: self.bounds, searchZoom: self.zoom)
		
		print("Total feature count: \(allFeatures.count)")

		let foundList = allFeatures.filter { feature in
			feature.tags.values.contains { value in
				value.lowercased().range(of: term.lowercased()) != nil
			}
		}

		print("Found \(foundList.count) with the search term \(term)")

		for found in foundList {
			print(found)
		}
	}
}
